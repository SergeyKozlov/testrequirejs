requirejs.config({
    "baseUrl": "",
    "paths": {
        "app": "",
        "jquery": "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min",
        "my_code1": "https://gitlab.com/SergeyKozlov/testrequirejs/-/raw/main/my_code1",
        "my_code2": "https://gitlab.com/SergeyKozlov/testrequirejs/-/raw/main/my_code1"

    },
    shim: {
        'jquery': { 
            exports: '$'
        },
        'my_code2': {
            deps: ['jquery', 'my_code1']
        }
        exports: "my_code1"
    }
});


// document.js file
define('global/document', ['global/window'], function (window) {
    return window.document;
});

// window.js file
define('global/window', [], function () {
    return window;
});

define('global/document', ['global/window'], function (window) {
    return window.global;
});


requirejs.onError = function (err) {
    console.log(err.requireType);
    if (err.requireType === 'timeout') {
        console.log('modules: ' + err.requireModules);
    }

    throw err;
};
